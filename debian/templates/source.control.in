Section: kernel
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Raphaël Hertzog <hertzog@debian.org>, Sophie Brun <sophie@offensive-security.com>
XSBC-Original-Maintainer: Debian Kernel Team <debian-kernel@lists.debian.org>
Standards-Version: 4.2.0
Build-Depends:
 debhelper-compat (= 12), dh-exec,
# used to run debian/bin/*.py
 python3:native,
 python3-jinja2:native,
# used by debian/rules.real to prepare the source
 quilt,
# used by debian/rules.real to build linux-doc and linux-headers
 cpio <!stage1>,
# used by upstream to compress kernel and by debian/rules.real to build linux-source
 xz-utils <!stage1>,
# used by debian/rules.real to build linux-perf and linux-support
 dh-python <!stage1>,
# used by upstream to build genksyms, kconfig, and perf
 bison <!stage1>, flex (>= 2.6.1-1.1~) <!stage1>,
Build-Depends-Arch:
# used by debian/rules.real to build udebs
 kernel-wedge (>= 2.102~) <!stage1 !pkg.linux.nokernel>,
# used by kernel-wedge (only on Linux, thus not declared as a dependency)
 kmod <!stage1 !pkg.linux.nokernel>,
# used by upstream to build include/generated/timeconst.h
 bc <!stage1 !pkg.linux.nokernel>,
# used by upstream to build signing tools and to process certificates
 libssl-dev:native <!stage1 !pkg.linux.nokernel>,
 libssl-dev <!stage1 !pkg.linux.notools>,
 openssl (>= 1.1.0-1~) <!stage1 !pkg.linux.nokernel>,
# used by upstream to build objtool (native for images; host arch for
# linux-kbuild), perf (host arch)
 libelf-dev:native <!stage1 !pkg.linux.nokernel>,
 libelf-dev <!stage1 !pkg.linux.notools>,
# used by upstream headers_install target and by debian/rules.d/tools/usb/usbip
 rsync,
 lz4 [amd64 arm64] <!stage1 !pkg.linux.nokernel>,
# used for bft debug info
 pahole <!stage1 !pkg.linux.nokernel> | dwarves:native (>= 1.16~) <!stage1 !pkg.linux.nokernel>,
Rules-Requires-Root: no
XS-Debian-Vcs-Git: https://salsa.debian.org/kernel-team/linux.git
XS-Debian-Vcs-Browser: https://salsa.debian.org/kernel-team/linux
Vcs-Git: https://gitlab.com/kalilinux/packages/linux.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/linux
Homepage: https://www.kernel.org/
